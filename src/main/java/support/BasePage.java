package support;

import managers.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends WebDriverManager {

    public void navigateToURL() throws Exception {
        getDriver();
        driver.navigate().to(getLoginURL());
    }

    public void waitUntilClickable(WebElement ele){
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until(ExpectedConditions.elementToBeClickable(ele));
    }

    public void clickElement(WebElement ele){
        waitUntilClickable(ele);
        try{
            ele.click();
        }catch(Exception e){
            jsClickElement(ele);
        }
    }

    public void jsClickElement(WebElement ele){
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",ele);
    }

    public void waitUntilVisible(WebElement ele){
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until(ExpectedConditions.visibilityOf(ele));
    }

    public WebElement findElementByText(String text){
        String xpath = "//div[text()='sometext']";
        String finalXpath = xpath.replace("sometext",text);
        return driver.findElement(By.xpath(finalXpath));
    }
}
