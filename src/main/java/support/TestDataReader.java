package support;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestDataReader {
    private static TestDataReader dataReader = null;
    private static Properties properties = null;
    private static String propertyFilePath;

    private static String getPropertyFilePath(){
        if(System.getProperty("os.name").toLowerCase().contains("win")){
            propertyFilePath = "src\\main\\resources\\testData.properties";
        }else propertyFilePath = "src/main/resources/testData.properties";
        return propertyFilePath;
    }

    private TestDataReader() {
        properties = loadProperties();
    }

    public static synchronized TestDataReader getDataReader(){
        if(dataReader == null){
            dataReader = new TestDataReader();
        }
        return dataReader;
    }

    private Properties loadProperties(){
        Properties testDataProperties = new Properties();
        BufferedReader reader;
        try{
            reader = new BufferedReader((new FileReader(getPropertyFilePath())));
            try{
                testDataProperties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
            throw new RuntimeException(("Properties file not found at " + propertyFilePath));
        }
        return testDataProperties;
    }

    public String getData(String key) throws Exception{
        String data = properties.getProperty(key);
        if(data != null){
            return data;
        }else{
            throw new Exception(String.format("The key %s does not exist", key));
        }
    }

    public String getBrowser() throws Exception{
        String env = null;
        if(System.getProperty("browser") != null){
            env = System.getProperty("browser");
        }else{
            env = getData("BROWSER");
        }
        return env;
    }
}
