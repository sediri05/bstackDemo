package pageObjectFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import support.BasePage;
import support.TestDataReader;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {PageFactory.initElements(driver,this);}

    @FindBy(id = "login-btn")
    WebElement loginBtn;

    @FindBy(id = "username")
    WebElement usernameField;

    @FindBy(id = "password")
    WebElement passwordField;

    public void login() throws Exception {
        clickElement(usernameField);
        clickElement(findElementByText(TestDataReader.getDataReader().getData("USERNAME")));
        clickElement(passwordField);
        clickElement(findElementByText(TestDataReader.getDataReader().getData("PASSWORD")));
        clickElement(loginBtn);
    }
}
