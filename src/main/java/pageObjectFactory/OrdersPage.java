package pageObjectFactory;

import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import support.BasePage;
import support.TestDataReader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OrdersPage extends BasePage {
    public OrdersPage(WebDriver driver) {PageFactory.initElements(driver,this);}

    @FindBy(xpath = "//*[contains(text(),'Delivered')]")
    WebElement deliveredDateValue;

    @FindBy(xpath = "//*[contains(text(),'Order placed')]/parent::div/following-sibling::div/span")
    WebElement orderPlacedDate;

    @FindBy(xpath = "//*[contains(text(),'Title')]/parent::div")
    WebElement productTitleValue;

    @FindBy(xpath = "//*[contains(text(),'Description')]/parent::div")
    WebElement productDesValue;

    @FindBy(xpath = "//div[contains(@class,'shipment')]//span[contains(text(),'$')]")
    WebElement individualPrice;

    @FindBy(xpath = "//*[contains(text(),'Ship to')]/parent::div/following-sibling::div/span")
    WebElement shipToValue;

    @FindBy(xpath = "//*[contains(text(),'Total')]/parent::div/following-sibling::div/span")
    WebElement totalPriceValue;



    public void verifyOrderDetails(String productName, String price) throws Exception {
        waitUntilVisible(deliveredDateValue);
        Assert.assertTrue(deliveredDateValue.getText().contains(getCurrentDate()),deliveredDateValue.getText() + " is different to today's date: " + getCurrentDate());
        Assert.assertTrue(orderPlacedDate.getText().contains(getCurrentDate()),orderPlacedDate.getText() + " is different to today's date: " + getCurrentDate());
        Assert.assertTrue(productTitleValue.getText().contains(productName),"Product title listed - " + productTitleValue.getText() + " is different to expected product title - " + productName);
        Assert.assertTrue(productDesValue.getText().contains(productName),"Product description listed - " + productTitleValue.getText() + " is different to expected product description - " + productName);
        Assert.assertEquals(shipToValue.getText(),(TestDataReader.getDataReader().getData("USERNAME")),"Ship to user listed - " + productTitleValue.getText() + " is different to the user used to login and buy item - " + TestDataReader.getDataReader().getData("USERNAME"));
        Assert.assertTrue(individualPrice.getText().contains(price),"Price listed on order screen - " + individualPrice.getText() + " is different to the price listed when buying - " + price);
        String roundedPrice = price.substring(0,price.indexOf("."));
        Assert.assertTrue(totalPriceValue.getText().contains(roundedPrice),"Total price listed on order screen - " + totalPriceValue.getText() + " is different to the rounded price listed when buying - " + roundedPrice);
    }


    private String getCurrentDate(){
        Date today = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        return fmt.format(today);
    }
}
