package pageObjectFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import support.BasePage;
import support.TestDataReader;

public class CheckoutPage extends BasePage {
    public CheckoutPage(WebDriver driver) {PageFactory.initElements(driver,this);}

    @FindBy(id = "firstNameInput")
    WebElement firstNameField;

    @FindBy(id = "lastNameInput")
    WebElement lastNameField;

    @FindBy(id = "addressLine1Input")
    WebElement addressField;

    @FindBy(id = "provinceInput")
    WebElement stateField;

    @FindBy(id = "postCodeInput")
    WebElement postCodeField;

    @FindBy(id = "checkout-shipping-continue")
    WebElement checkoutSubmitBtn;

    @FindBy(xpath = "//button[contains(text(),'Continue Shopping')]")
    WebElement continueShoppingBtn;

    @FindBy(xpath = "//h5[contains(@class,'product-title')]")
    WebElement checkoutProdTitle;

    @FindBy(xpath = "//span[contains(text(),'$')]")
    WebElement checkoutPrice;


    public void inputShippingDetails() throws Exception {
        waitUntilClickable(checkoutSubmitBtn);
        firstNameField.sendKeys(TestDataReader.getDataReader().getData("FIRSTNAME"));
        lastNameField.sendKeys(TestDataReader.getDataReader().getData("LASTNAME"));
        addressField.sendKeys(TestDataReader.getDataReader().getData("ADDRESS"));
        stateField.sendKeys(TestDataReader.getDataReader().getData("STATE"));
        postCodeField.sendKeys(TestDataReader.getDataReader().getData("POSTCODE"));
        clickElement(checkoutSubmitBtn);
    }

    public void verifyCheckoutProduct(String title,String buyPrice) throws Exception {
        waitUntilClickable(continueShoppingBtn);
        checkoutPrice.getText().contains(buyPrice);
        checkoutProdTitle.getText().equalsIgnoreCase(title);
    }

    public void navigateToHomePageAfterPurchase(){
        clickElement(continueShoppingBtn);
    }
}
