package pageObjectFactory;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import support.BasePage;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {PageFactory.initElements(driver,this);}

    @FindBy(id="signin")
    WebElement signInNavBtn;

    @FindBy(id = "orders")
    WebElement ordersNavBtn;

    @FindBy(xpath = "//*[text()='One Plus 8T']/following-sibling::div[text()='Add to cart']")
    WebElement onePlus8TAddToCart;

    @FindBy(xpath = "//div[text()='SUBTOTAL']/following-sibling::div/p")
    WebElement priceShownInBagWindow;

    @FindBy(xpath = "//div[text()='Checkout']")
    WebElement checkoutBtn;

    WebElement addItemToCart;
    WebElement pricePart1;
    WebElement pricePart2;

    public void navigateToLoginInPage(){
        clickElement(signInNavBtn);
    }

    public String addItemToCart(String item){
        waitUntilClickable(onePlus8TAddToCart);
        setItemElement(item);
        String buyPrice = pricePart1.getText() + pricePart2.getText();
        clickElement(addItemToCart);
        waitUntilClickable(checkoutBtn);
        Assert.assertTrue(priceShownInBagWindow.getText().contains(buyPrice),"Price shown in the Bag window: " + priceShownInBagWindow.getText() + " is not the same as the price shown on the home page: $" + buyPrice);
        clickElement(checkoutBtn);
        return buyPrice;
    }

    private void setItemElement(String text){
        String itemXpath = "//*[text()='sometext']/following-sibling::div[text()='Add to cart']";
        String pricePart1Xpath = "//*[text()='sometext']/following-sibling::div//b";
        String pricePart2Xpath = "//*[text()='sometext']/following-sibling::div//b/following-sibling::span";
        addItemToCart = driver.findElement(By.xpath(itemXpath.replace("sometext",text)));
        pricePart1 = driver.findElement(By.xpath(pricePart1Xpath.replace("sometext",text)));
        pricePart2 = driver.findElement(By.xpath(pricePart2Xpath.replace("sometext",text)));
    }

    public void  navigateToOrdersPage(){
        clickElement(ordersNavBtn);
    }
}
