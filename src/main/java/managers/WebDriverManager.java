package managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import support.TestDataReader;

import java.util.HashMap;
import java.util.Map;

import static io.github.bonigarcia.wdm.WebDriverManager.*;

public class WebDriverManager {

    public static WebDriver driver;
    public static String driverType;
    Map<String, Object> prefs = new HashMap<String, Object>();

    public WebDriver getDriver() throws Exception{
        driver = createLocalDriver();
        return driver;
    }

    public static String getLoginURL() throws Exception{
        return TestDataReader.getDataReader().getData("URL");
    }

    private WebDriver createLocalDriver() throws Exception{
        String driverType = TestDataReader.getDataReader().getBrowser();
        switch(driverType.toLowerCase()){
            case "chrome":
                chromedriver().setup();
                ChromeOptions cOpt = new ChromeOptions();
                cOpt.addArguments("--disable-infobars");
                cOpt.addArguments("--start-maximized");
                cOpt.addArguments("--ignore-ssl-errors=yes");
                cOpt.addArguments("--ignore-certificate-errors");
                cOpt.addArguments("--disable-notifications");
                cOpt.addArguments("--no-sandbox");
                cOpt.setHeadless(true);
                driver = new ChromeDriver(cOpt);
                break;
            case "firefox":
                firefoxdriver().setup();
                FirefoxOptions fOpts = new FirefoxOptions();
                fOpts.setHeadless(true);
                driver = new FirefoxDriver(fOpts);
                break;
            /*case "edge":
                edgedriver().setup();
                driver = new EdgeDriver();
                break;*/
            default:
                System.out.println("Error: Browser \"" + driverType + "\" specified is not correct");
                System.out.println("Please mention one of the required browser as below \n\tbrowser = chrome \n\tbrowser = edge \n\tbrowser = firefox \nin the properties file");
        }
        return driver;
    }
}
