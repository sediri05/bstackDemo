@TestAll @TestUI
Feature: Buy an item

  Scenario: Buy an item and verify order history
    Given User navigates to Demo site
    And User logs in
    When User purchases "One Plus 8T"
    Then User can see order in order history