@TestAll @TestAPI
Feature: API tests on browser stack demo site


  Scenario: Verify Sign In API - Positive
    Given User sets the base API request for "SignIn"
    When User hits the SignIn API using "valid" credentials
    Then API returns "200" status code


  Scenario: Verify Sign In API - Negative
    Given User sets the base API request for "SignIn"
    When User hits the SignIn API using "invalid" credentials
    Then API returns "422" status code
    And error message is returned


  Scenario: Verify Orders API
    Given User sets the base API request for "Orders"
    When User hits the Orders API using valid credentials
    Then API returns "404" status code


  Scenario: Verify Products API
    Given User sets the base API request for "Products"
    When User hits the Products API using valid credentials
    Then API returns "200" status code
    And Verify "iPhone 12 Pro Max" is present