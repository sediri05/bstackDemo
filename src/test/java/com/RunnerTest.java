package com;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"classpath:features"},
glue = {"stepDefs"},
plugin = {"pretty",
        "html:target/Results/Reports/HTMLReports/report.html",
        "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
        "json:src/main/java/support/cucumber.json",
        "pretty:pretty-report.log"
},
        monochrome = true
)

public class RunnerTest {
}
