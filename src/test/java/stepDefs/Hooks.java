package stepDefs;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import support.BasePage;


public class Hooks extends BasePage {


    @AfterStep
    public void addScreenshot(Scenario scenario){

        if(scenario.isFailed()){
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/png","image");
        }
    }

    @After(value = "@TestUI", order=0)
    public void afterAll() {
        if(driver !=null){
            driver.quit();
        }
    }
}
