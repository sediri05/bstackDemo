package stepDefs;

import io.cucumber.java.en.*;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import support.TestDataReader;

import java.util.HashMap;


public class APIStepDefs {
    RequestSpecification request;
    Response response;
    String userName;
    String password;

    @Given("User sets the base API request for {string}")
    public void initKey(String apiType) throws Exception {
        RestAssured.baseURI = TestDataReader.getDataReader().getData("BASEURI");
        RestAssured.basePath = TestDataReader.getDataReader().getData("BASEPATH").concat(apiType.toLowerCase());
        userName = TestDataReader.getDataReader().getData("USERNAME");
        password = TestDataReader.getDataReader().getData("PASSWORD");
    }

    @When("User hits the SignIn API using {string} credentials")
    public void userHitsSignInAPI(String validity) throws Exception {
        if(validity.equalsIgnoreCase("invalid")){
            userName=userName+1;
        }
        HashMap map = new HashMap();
        map.put("userName",userName);
        map.put("password",password);
        request = given().contentType(ContentType.JSON).body(map);
        response = request.when().post();
    }

    @When("User hits the Products API using valid credentials")
    public void userHitsProductsAPI() throws Exception {
        request = given().queryParam("userName",TestDataReader.getDataReader().getData("USERNAME"));
        response = request.when().get();
    }

    @When("User hits the Orders API using valid credentials")
    public void userHitsOrdersAPI() throws Exception {
        request = given().queryParam("userName",TestDataReader.getDataReader().getData("USERNAME"));
        response = request.when().get();
    }

    @Then("API returns {string} status code")
    public void apiResponseVerification(String statusCode){
        response.then().statusCode(Integer.parseInt(statusCode));
    }

    @And("Verify {string} is present")
    public void verifyProductResponse(String productName){
        response.then().body("products.title",hasItems(productName));
    }

    @And("error message is returned")
    public void verifyErrorMsg(){
        response.then().assertThat().body("errorMessage",equalToIgnoringCase("Invalid Username"));
    }
}

