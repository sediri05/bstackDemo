package stepDefs;

import io.cucumber.java.en.*;
import pageObjectFactory.CheckoutPage;
import pageObjectFactory.HomePage;
import pageObjectFactory.LoginPage;
import pageObjectFactory.OrdersPage;
import support.BasePage;

public class BuyItemUIStepDef extends BasePage {
    String productTitle;
    HomePage homePage;
    LoginPage loginPage;
    CheckoutPage checkoutPage;
    String buyPrice;
    OrdersPage ordersPage;


    @Given("User navigates to Demo site")
    public void navigateToSite() throws Exception {
        navigateToURL();
    }

    @And("User logs in")
    public void login() throws Exception {
        homePage = new HomePage(driver);
        homePage.navigateToLoginInPage();
        loginPage = new LoginPage(driver);
        loginPage.login();
    }

    @When("User purchases {string}")
    public void purchaseItem(String item) throws Exception {
        productTitle = item;
        buyPrice = homePage.addItemToCart(item);
        checkoutPage = new CheckoutPage(driver);
        checkoutPage.inputShippingDetails();
        checkoutPage.verifyCheckoutProduct(productTitle,buyPrice);
        checkoutPage.navigateToHomePageAfterPurchase();
    }

    @Then("User can see order in order history")
    public void orderHistory() throws Exception {
        homePage.navigateToOrdersPage();
        ordersPage = new OrdersPage(driver);
        ordersPage.verifyOrderDetails(productTitle,buyPrice);
    }
}
